# Übersicht und Verlinkung weiterer öffentlich verfügbarer FDM-Trainingsmaterialien. <br> _Overview and links of further publicly available RDM training materials._

## Selbstlernmaterialien <br> _Self-study trainings_

| Titel | Link | Autor / Urheber | Format | Bemerkung |
| ------ | ------ | ------ | ------ | ------ |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> :de: Persönliches Datenmanagement <br> :gb: Personal Data Management | :de: [http://doi.org/10.5281/zenodo.3724164](http://doi.org/10.5281/zenodo.3724164    ) <br> :gb: [http://doi.org/10.5281/zenodo.4013758](http://doi.org/10.5281/zenodo.4013758    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> :de: Datenmanagementplan <br> :gb: Data Management Plan | :de: [http://doi.org/10.5281/zenodo.3974562](http://doi.org/10.5281/zenodo.3974562    ) <br> :gb: [http://doi.org/10.5281/zenodo.3974583](http://doi.org/10.5281/zenodo.3974583    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> :de: Anforderungen der Förderer für FDM <br> :gb: Requirements of Funding Agencies for RDM | :de: [http://doi.org/10.5281/zenodo.3734879](http://doi.org/10.5281/zenodo.3734879    ) <br> :gb: [http://doi.org/10.5281/zenodo.3735838](http://doi.org/10.5281/zenodo.3735838    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> Umgang mit großen Datenmengen | [http://doi.org/10.5281/zenodo.3702113](http://doi.org/10.5281/zenodo.3702113    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> Repositorien | [http://doi.org/10.5281/zenodo.3706761](http://doi.org/10.5281/zenodo.3706761    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> Datenrecherche | [http://doi.org/10.5281/zenodo.3466206](http://doi.org/10.5281/zenodo.3466206    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> Metadaten | [http://doi.org/10.5281/zenodo.2660187](http://doi.org/10.5281/zenodo.2660187    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> Forschungsdaten-Policy | [http://doi.org/10.5281/zenodo.2613364](http://doi.org/10.5281/zenodo.2613364    ) | TU9 Bibliotheken | Foliensatz | |
| FDM Trainingsmaterialien der TU9 Bibliotheken: <br> :de: Software als Forschungsdaten <br> :gb: Software as research data | :de: [http://doi.org/10.5281/zenodo.2611303](http://doi.org/10.5281/zenodo.2611303    ) <br> :gb: [http://doi.org/10.5281/zenodo.2611314](http://doi.org/10.5281/zenodo.2611314    ) | TU9 Bibliotheken | Foliensatz | |
| :de: Einführung ins Forschungsdatenmanagement <br> :gb: Introduction to Research Data Management | :de: [https://doi.org/10.5281/zenodo.4644229](https://doi.org/10.5281/zenodo.4644229    ) <br> :gb: [https://doi.org/10.5281/zenodo.4905952](https://doi.org/10.5281/zenodo.4905952    ) | Jessica Rex | Foliensatz | |
| :gb: Research Data Management 1 day workshop | [https://doi.org/10.5281/zenodo.4562630](https://doi.org/10.5281/zenodo.4562630    ) | Dr. Sara El-Gebali | Foliensatz, Literaturübersicht | |
| :de: Train-the-Trainer Konzept zum Thema forschungsdatenmanagement <br> :gb: Train-the-Trainer Concept on Research Data Management | :de: [https://doi.org/10.5281/zenodo.4322849](https://doi.org/10.5281/zenodo.4322849    ) <br> :gb: [https://doi.org/10.5281/zenodo.4071471](https://doi.org/10.5281/zenodo.4071471    ) | UAG Schulung/Fortbildung; <br> Katarzyna Biernacka; Maik Bierwirth; Petra Buchholz; et.al. | Foliensatz, Lehrbuch, ergänzende Materalien |  |
| Metadaten <br> - Metadaten: eine Einführung <br> - Metadatenarten und -schemata <br> - Beschreibung von Forschungsdaten am Beispiel von DataCite <br> - Normdaten | [https://www.youtube.com/playlist?list=PL9ZaxDtkP6tz9dIpvNyLNj_xsUgdkMdy7](https://www.youtube.com/playlist?list=PL9ZaxDtkP6tz9dIpvNyLNj_xsUgdkMdy7) | Forschungsdatenmanagement Bayern | Video (Playlist mit 4 Videos) |  |
| FDM-Serviceteam der LUH: <br> Digitale Forschungsdaten managen - Grundlagen, Tipps und Tricks | [Link Website :arrow_upper_right:](https://www.fdm.uni-hannover.de/de/veranstaltungen/online-einfuehrungskurs-digitale-forschungsdaten-managen-grundlagen-tipps-und-tricks/) | FDM-Serviceteam der LUH | Selbstlernkurs + Foliensatz |  |
| FDM-Serviceteam der LUH: <br> Umgang mit personenbezogenen Forschungsdaten - Rechtliche Grundlagen, Methoden, Hilfsmittel | [Link Website :arrow_upper_right:](https://www.fdm.uni-hannover.de/de/veranstaltungen/kurs-persbez-forschungsdaten/) | FDM-Serviceteam der LUH | Selbstlernkurs + Foliensatz |  |
| FDM-Serviceteam der LUH: <br> Forschungsdaten dokumentieren und publizieren | [Link PDF :arrow_upper_right:](https://www.fdm.uni-hannover.de/fileadmin/fdm/Dokumente/Schulungsunterlagen/Schulungsunterlagen_FDM_VertiefungDatendokumentationPublikation_Folien.pdf) | FDM-Serviceteam der LUH | Foliensatz |  |
| FDM-Serviceteam der LUH: <br> Datemanagementpläne als Instrument des FDM | [Link PDF :arrow_upper_right:](https://www.fdm.uni-hannover.de/fileadmin/fdm/Dokumente/Schulungsunterlagen/Schulungsunterlagen_FDM_VertiefungDMP_Folien.pdf) | FDM-Serviceteam der LUH | Foliensatz |  |
| FDM-Serviceteam der LUH: <br> Datenorganisation und Projektablage | [Link PDF :arrow_upper_right:](https://www.fdm.uni-hannover.de/fileadmin/fdm/Dokumente/Schulungsunterlagen/Schulungsunterlagen_FDM_VertiefungDatenorganisation_Folien.pdf) | FDM-Serviceteam der LUH | Foliensatz |  |
| Videos zum Forschungsdatenmanagement: <br> - Forschungsdaten leben länger <br> - Forschungsdaten und ihre Metadaten <br> - Datenmanagement nach Plan <br> - Inhalte eines Datenmanagementplans (DMP) <br> - Persistent Identifiers (PID) <br> - Forschungsdaten zu Publikation | :de: [Link UB RWTH :arrow_upper_right:](https://www.ub.rwth-aachen.de/cms/ub/Studium/E-Learning/~bkgez/Forschungsdaten/), <br> :de:/:gb: [YouTube Playlist :arrow_upper_right:](https://www.youtube.com/playlist?list=PL0eaiqVqG1ovx-A_rGpz76nylfLjd9auR) | Universitätsbibliothek RWTH Aachen / Medien für die Lehre RWTH Aachen | Videoserie | <sub>Dominik Schmitz, Daniela Hausen, Benedikt Magrean, Ute Trautwein-Bruns, Jürgen Windeck. Forschungsdaten leben länger…. RWTH Aachen University. 2018. Verfügbar unter DOI: 10.18154/RWTH-2018-220883 . Creative Commons-Lizenz mit Quellenangabe (Wiederverwendung erlaubt)</sub> |
| NFDI4Ing Knowledge Base: Research Software Development | [https://nfdi4ing.pages.rwth-aachen.de/knowledge-base/ :arrow_upper_right:](https://nfdi4ing.pages.rwth-aachen.de/knowledge-base/) | NFDI4Ing Base Service S-2 | Webseite | Anleitungen und Erfahrungssammlung mit Fokus auf Software Development | 
| Read the docs: Data Quality Metrics | [https://data-quality-metrics.readthedocs.io/en/main/ :arrow_upper_right:](https://data-quality-metrics.readthedocs.io/en/main/) | NFDI4Ing TA Golo | Webseite | Übersicht verschiedener Datenqualitätsmetriken mit Definition und Codebeispielen | 
| Metadata Profile Service Documentation | [https://aims.pages.rwth-aachen.de/public/documentation/user-guide/platform/about/ :arrow_upper_right:](https://aims.pages.rwth-aachen.de/public/documentation/user-guide/platform/about/) | AIMS Team | Documentation | Dokumentation, Code und Anwendungsbeispiele zum [Metadata Profile Service https://profiles.nfdi4ing.de :arrow_upper_right:](https://profiles.nfdi4ing.de)
<!-- | Titel | Link | Autor | Typ | Bemerkung | -->





## Sie vermissten hier etwas? <br> _Did you miss anything here?_

Nennen Sie uns Ergänzungen oder Korrekturen gerne als [Issue](https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien/-/issues/new) - vielen Dank! (Anmeldung erforderlich)
<br>
_Please feel free to send us additional training materials or corrections as an [issue](https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien/-/issues/new) - thank you in advanced! (Sign in required)_ 

### Nachnutzung
Bei einer Nachnutzung von diesen Unterlagen geben Sie bitte, abhängig ob die Materialien verändert wurden oder nicht, die folgende Namensnennung an:

<details><summary>Die Materialien wurden nicht verändert</summary>

Für eine vollständige Namensnennung empfehlen wir Ihnen, wenn Sie Materialien von diesem GitLab nachnutzen möchten, folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien/-/blob/main/LICENSE_Unver%C3%A4ndert

</details>

<details><summary>Die Materialien wurden verändert</summary>

Für eine vollständige Namensnennung empfehlen wir Ihnen, wenn Sie Materialien von diesem GitLab nachnutzen möchten, folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt und verändert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
https://git.rwth-aachen.de/nfdi4ing/education/uebersicht-oeffentlicher-materialien/-/blob/main/LICENSE_Ver%C3%A4ndert

</details>

Weitere Infos zu Lizenzen und der Nachnutzung der hier bereitgestellten Materialien finden Sie [hier](https://git.rwth-aachen.de/groups/nfdi4ing/education/-/wikis/Lizenzen).

### Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Diese Seite und ihre Inhalte sind lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.
